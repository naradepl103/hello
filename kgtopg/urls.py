"""newkgtopg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.urls import include, path
	2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from rest_framework import routers
from base import views



router = routers.DefaultRouter()




admin.site.site_header = "KGtoPG Administration"
admin.site.site_title = "KGtoPG Admin Portal"
admin.site.index_title = "Welcome to KGtoPG"
admin.site.site_url  =  "https://kgtopg.com/"




urlpatterns = [
	path('admin/', admin.site.urls),
	path('',include(router.urls)),
	
	path('schooler/',views.SchoolerRelView.as_view(),name = 'chapterfind'),

	# path('std/', views.StudentAttendanceView.as_view()),
	# path('task/<str:task_id>/', views.TaskView.as_view(), name='task'),
	# path('onlinestdatt/',views.OnlineStudentView.as_view()),
	##path('camerafeed/',views.camerafeedView, name='camera'),
	# path('camerafeed2/', views.livefe, name='camera2'),
	# path('dataset/', views.CreateImageDatasetView.as_view()),
	# path('trainimage/', views.TrainImages.as_view()),

]
