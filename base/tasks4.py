from celery import shared_task
import os
import datetime
from datetime import date

import  cv2
from kgtopg.settings import BASE_DIR
from celery.contrib import rdb
import requests
import shutil # to save it locally
from .utils import print_definitions, imreadx, face_descriptor, load_fr_model, face_detection, face_scores
from .utils import session_descriptors, is_face, show_face

from base.models import Student,StudentFace,StudentAttendance,ClassroomSnapshot,Notification

import dlib
from PIL import Image
from skimage import io
import numpy as np

#---------------------------------------------------------------------------------------------------#
#---------------Global Assignments------------------------------------------------------------------#
fd_method   = 0               # face detection method (0:HOG, 1: CNN)
fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
uninorm     = 1               # 1 means descriptor has norm = 1
theta       = 0.6             # threshold for cosine similarity
print_scr   = 1               # print scores
show_img    = 0               # show images
D = None					  # session descriptor

print("[facer] : ----------------------------------------------------------------") 
# print_definitions(fd_method,fr_method,sc_method,uninorm,theta)

print("[facer] : loading face recognition model...") 
fr_model = load_fr_model(fr_method) 

def detect_face(image):
	# Create a face detector
	face_detector = dlib.get_frontal_face_detector()
	# Run detector and get bounding boxes of the faces on image.
	detected_faces = face_detector(image, 1)
	face_frames = [(x.left(), x.top(),x.right(), x.bottom()) for x in detected_faces]
	return face_frames

def face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path):
	detected = False

	r = requests.get(image_url, stream = True)
	if r.status_code == 200:
		# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
		r.raw.decode_content = True
		# Open a local file with wb ( write binary ) permission.
	#	tmp_path = "/home//vijay/prog/python/kgtopg-platform/temp/"
		tmp_path = "/tmp/"
		with open(tmp_path+filename,'wb') as f:
			shutil.copyfileobj(r.raw, f)
		print('Image sucessfully Downloaded: ',filename)
	
	img_query = tmp_path+filename
	
	desc_q = None
	
	image = io.imread(img_query)
	
	detected_faces = detect_face(image)
	
	for n, face_rect in enumerate(detected_faces):
		face = Image.fromarray(image).crop(face_rect)  #facecrop
		im = np.array(face)
		desc_q = face_descriptor(im,fr_method,fr_model,uninorm)

	img_session = class_img_path
	# img_session = os.path.join(BASE_DIR,"base/test2.jpg") # session file image (picture of the classroom with students)

	# computation of scores between query face and every face of the session
	t = face_scores(D,desc_q,sc_method,print_scr)

	# find if query face is in session image
	i = is_face(t,theta,sc_method)

	if i>=0:
		print("[facer] : face #" + str(i) + " was detected with score = " + str(t[i]))
		std_att_inst.has_attended_final = 1
		std_att_inst.save()
		detected = True
	else:
		print("[facer] : face in query image not detected in session image")
		std_att_inst.has_attended_final = 0
		std_att_inst.save()

	return detected

@shared_task

def make_thumbnails(instituteId, staffId,subjectId,classroomId):
	fd_method   = 0               # face detection method (0:HOG, 1: CNN)
	fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
	sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
	uninorm     = 1               # 1 means descriptor has norm = 1
	theta       = 0.6             # threshold for cosine similarity
	print_scr   = 1               # print scores
	show_img    = 0               # show images
	print("[facer] : ----------------------------------------------------------------") 
	print_definitions(fd_method,fr_method,sc_method,uninorm,theta)
	print("[facer] : loading face recognition model...") 

	fr_model = load_fr_model(fr_method) 

	std_id = Student.objects.filter(institute_id=instituteId,classroom_id=classroomId).values_list('student_id')

	# image_url = StudentFace.objects.get(student_id=std_id).values('student_face_url')
	# image_url = "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTE5NDg0MDU0OTM2NTg1NzQz/tom-cruise-9262645-1-402.jpg"
	# print(std_id)
	#  rdb.set_trace()
	today = date.today()
	
	# Take classroom snapshot url and create file from it
	# class_img_session = ClassroomSnapshot.objects.filter(classroom_id=classroomId,created_date=today).order_by('-timestamp')[0]
	# class_image_url = class_img_session.snapshot_url
	# class_rtsp_url = class_img_session.rtsp_url
	#----------------local testing url-----------------#
	class_rtsp_url = "rtsp://admin:MOJIOD@192.168.10.100"
	#--------------------------------------------------#
	stream = cv2.VideoCapture(class_rtsp_url)
	r, f = stream.read()
	gray = cv2.cvtColor(f,cv2.COLOR_BGR2GRAY)
	cv2.imwrite("/tmp/opencv1.jpg", f)
	stream.release()
		# class_img_filename = class_img_session.snapshot_url.split("?")[0].split("/")[-1]
	# r = requests.get(class_image_url, stream = True)
	# if r.status_code == 200:
	# 	# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
	# 	r.raw.decode_content = True
	# 	# Open a local file with wb ( write binary ) permission.
	# 	tmp_path = "/tmp/"
	# 	with open(tmp_path+class_img_filename,'wb') as f:
	# 		shutil.copyfileobj(r.raw, f)
		# print('Image sucessfully Downloaded: ',class_img_filename)
	tmp_path = "/tmp/"
	class_img_filename = "opencv1.jpg"
	class_img_path = tmp_path+class_img_filename
	# classroom snapshot FILE now available..
	print('Classroom snapshot file now available..');

	# print("[facer] : loading face recognition model...") 
	# fr_model = load_fr_model(fr_method) 

	# filename = sf.student_face_url.split("?")[0].split("/")[-1] 

	# Iterating each student..
	for index in range(std_id.__len__()):
		try:
			studnt = std_id[index]
			# rdb.set_trace()
			# attendance query
			std_att_inst = StudentAttendance.objects.get(student_id=std_id[index][0],staff_id=staffId,
				subject_id=subjectId,classroom_id=classroomId,created_date=today)
			# print(std_att_inst)

			# check if already record exists with attended status..
			if std_att_inst.has_attended:
				pass # should not be passed????
			else:
				# perform face recognization
				try:
					sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
					stu_faces = StudentFace.objects.filter(student_id=studnt[0])

					for s_index in range(stu_faces.__len__()) :
						# reassign
						sf = stu_faces[s_index]
						image_url = sf.student_face_url
						filename = sf.student_face_url.split("?")[0].split("/")[-1]
						detected = face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path)
						if detected:
							break
						else:
						 	continue

				except :
					std_att_inst.has_attended_final = 0
					std_att_inst.save()
					continue
					
		except StudentAttendance.DoesNotExist:
			# print('Exception in getting record %s', e)
			std_att_inst = StudentAttendance.objects.create(student_id=std_id[index][0],staff_id=staffId,subject_id=subjectId,classroom_id=classroomId,created_date=today,attendance_date=today,has_attended=0,has_attended_final=0,on_leave=0,timestamp=datetime.datetime.now())
			# sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
			# image_url = sf.student_face_url
			# filename = sf.student_face_url.split("?")[0].split("/")[-1]
			# face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename)
			try:
				sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]

				stu_faces = StudentFace.objects.filter(student_id=studnt[0])

				for s_index in range(stu_faces.__len__()) :
					# reassign
					sf = stu_faces[s_index]
					image_url = sf.student_face_url
					filename = sf.student_face_url.split("?")[0].split("/")[-1]
					detected = face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path)
					if detected:
						break
					else:
						continue
			except :
				std_att_inst.has_attended_final = 0
				std_att_inst.save()
				continue
			
	# save and generate notification to staff
	
	try:
		notif_url = "teacher-classroom-attendance?id=" + classroomId +"&subid=" + subjectId
		print("notification url is " + notif_url)
		notif= Notification.objects.create(from_id=0,login_type_id = 102 ,to_id = staffId ,notif_title = "Attendance Complete" , notif_message ="Attendance for your class is completed. Please check.", notif_for_url = notif_url, notif_for = "attendance",is_read = 0,is_notified = 0,created_date = today,timestamp = datetime.datetime.now())
	except Exception as e:
		print("notification cannot be saved " + str(e))

	return True


@shared_task

def online_std_att(studentId,instituteId, staffId,subjectId,classroomId,student_url):
	
	today = date.today().isoformat()  
	
	tmp_path = "/tmp/"
	
	r = requests.get(student_url, stream = True)
	
	if r.status_code == 200:
		# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
		r.raw.decode_content = True
		# Open a local file with wb ( write binary ) permission.
	#	tmp_path = "/home//vijay/prog/python/kgtopg-platform/temp/"
		filename = student_url.split("?")[0].split("/")[-1]
		with open(tmp_path+filename,'wb') as f:
			shutil.copyfileobj(r.raw, f)
		print('Image sucessfully Downloaded: ',filename)
	

	img_session = tmp_path + filename

	print("reading session image")
	
	S = imreadx(img_session,show_img)
	
	print("[facer] : detecting faces in session image " + img_session + "...")
	
	faces  = face_detection(S,fd_method)
	
	print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

	# computation of descriptors in detected faces of session image
	print("[facer] : finding "  + " in session image ...")
	
	D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

	print('student snapshot file now available..');

	
	try:
		# attendance query		
		std_att_inst = StudentAttendance.objects.get(student_id=studentId,staff_id=staffId,
				subject_id=subjectId,classroom_id=classroomId,created_date=today)
		
		if std_att_inst.has_attended == 1:
			try:
				stu_faces = StudentFace.objects.filter(student_id=studentId)
				for s_index in range(stu_faces.__len__()) :
					# reassign
					sf = stu_faces[s_index]
					image_url = sf.student_face_url
					filename = sf.student_face_url.split("?")[0].split("/")[-1]
					detected = face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path)
					if detected:
						break
			except Exception as e:
				print("Student faces objects %s",e)
		else:
			pass
	except Exception as e:
		print('Exception in getting attendance record %s', e)
		
	try:
		notif_title = "Attendance Complete"
		notif_msg = "Attendance for your student is completed. Please check."
		notif_url = "teacher-classroom-attendance?id={0}&subid={1}".format(classroomId, subjectId)
		print("notification url is %s ", notif_url)
		# notif= Notification.objects.create(from_id=0,login_type_id = 102 ,to_id = staffId ,notif_title = "Attendance Complete" , notif_message ="Attendance for your class is completed. Please check.", notif_for_url = notif_url, notif_for = "attendance",is_read = 0,is_notified = 0,created_date = today,timestamp = datetime.datetime.now())
		SQL = " INSERT INTO notification(login_type_id, from_id, to_id, notif_title, notif_message, notif_for_url, notif_for, is_read, is_notified, created_date, timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
		data = (103, 0, studentId, notif_title, notif_msg, notif_url, "attendance", 0, 1, today, datetime.datetime.now())

		mycursor.execute(SQL, data)
	except Exception as e:
		print("notification cannot be saved %s ", e)

	return True