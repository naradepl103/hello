from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.conf import settings
from .models import AdminUsers,User,LoginType,AdminRoles,Module,Institute,SchoolerPackage,InstituteBoard,UserModuleRel,SchoolerPackageRel,SchoolerPackage,SchoolerUserRel,Notification,Student, Staff
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin.options import InlineModelAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm,ProductAdminForm,PackageAdminForm,SchlrUserRelAdminForm  #,SchoolerPackageAdminForm
from django.core.mail import send_mail



# #=======================================================================================#
def send_email(sendermail):
	
#   # email = EmailMessage(
# 	  # 'Title',
# 	  # (ConsultSerializer.name, ConsultSerializer.email, ConsultSerializer.phone),
# 	  # ('Name', 'venkat.depl.it@gmail.com', '0'),
# 	  # 'cto.depl@gmail.com',
# 	  # ['cto.depl@gmail.com']
#   # )


#   # email.attach_file(ConsultSerializer.file)
  body = '''Congratulations!! Your Account is activated.Click the link below to login\r\n
http://kgtopg.com/
		  '''
  send_mail(
	  'Account Activated',
	  body,
	  'admin@kgtopg.com',
	  ['harsha.depl.python@gmail.com',sendermail],
	  fail_silently=False,
  )
# #=======================================================================================#

# class ArticleInline(admin.TabularInline):
#     model = AdminRoles
#     fields = ['user_role_name']

class CustomUserAdmin(UserAdmin):
	add_form = CustomUserCreationForm
	form = CustomUserChangeForm
	model = AdminUsers
	# inlines = [ArticleInline, ]
	list_display = ('email', 'is_staff', 'is_active','admin_role_id')
	list_filter = ('email', 'is_staff', 'is_active',)
	fieldsets = (
		(None, {'fields': ('email', 'password')}),
		('Permissions', {'fields': ('is_staff', 'is_active')}),
		('Roles', {'fields': ('admin_role_id',)}),
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active','admin_role_id')}
		),
	)
	search_fields = ('email',)
	ordering = ('email',)

	


	def has_add_permission(self, request):
		if request.user.is_superuser:
			return True
		else:
			return False
	def has_change_permission(self, request, obj=None):
		if request.user.is_superuser:
			return True
		else:
			return False
	def has_module_permission(self, request):
		return True


from base.forms import InsituteChangeForm

class InstituteAdmin(admin.ModelAdmin):
	# form = InsituteChangeForm
	list_display = ['institute_email','institute_status','institute_name']  #'package_name',
	actions = ['Activate']
	# def package_name(self,obj):
	# 	# import pdb;pdb.set_trace()
	# 	schlr = SchoolerPackageRel.objects.get(institute_id=obj.institute_id)
	# 	pkg = SchoolerPackage.objects.get(schooler_package_id=schlr.schooler_package_id)
	# 	return pkg.package_name 

	list_per_page =10
	def Activate(self,request, queryset):
		status = list(queryset.values_list('institute_id','institute_status','institute_email'))
		# inst = queryset.model.objects.get(institute_id=state[0])
		for state in status:
			if state[1] == False:
				# send_email(state[2])
				inst = queryset.model.objects.get(institute_id=state[0])
				inst.institute_id=True
				inst.save()
				# qs= queryset
				# self.make_published(self,request,queryset)
				message = 'pass'
			else:
				message = 'fail'
	Activate.short_description = 'Activate'

# class InstituteAdmin(admin.ModelAdmin):
# 	form = InsituteChangeForm
# 	list_display = ['package_name','institute_email','institute_status','institute_name']
# 	actions = ['Activate']
# 	def package_name(self,obj):
# 		# import pdb;pdb.set_trace()
# 		schlr = SchoolerPackageRel.objects.get(institute_id=obj.institute_id)
# 		pkg = SchoolerPackage.objects.get(schooler_package_id=schlr.schooler_package_id)
# 		return pkg.package_name 

# 	list_per_page =10
# 	def Activate(self,request, queryset):
# 		status = list(queryset.values_list('institute_id','institute_status','institute_email'))
# 		# inst = queryset.model.objects.get(institute_id=state[0])
# 		for state in status:
# 			if state[1] == False:
# 				# send_email(state[2])
# 				inst = queryset.model.objects.get(institute_id=state[0])
# 				inst.institute_id=True
# 				inst.save()
# 				# qs= queryset
# 				# self.make_published(self,request,queryset)
# 				message = 'pass'
# 			else:
# 				message = 'fail'
# 	Activate.short_description = 'Activate'





class ModuleAdmin(admin.ModelAdmin):
	# def save_model(self, request, obj, form, change):
	# 	file_path = '/home/depl/downloads'
	# 	task = make_thumbnails.delay(file_path, thumbnails=[(128, 128)])
	# 	super(ModuleAdmin, self).save_model(request, obj, form, change)
	def has_add_permission(self, request):
		try:
			# import pdb;pdb.set_trace()
			if request.user.is_superuser:
				return True
			if request.user.admin_role_id.user_role_name == 'agent':
				return True
		except :
			return False
	def has_change_permission(self, request, obj=None):
		try:
			if request.user.is_superuser:
				return True
			if request.user.admin_role_id.user_role_name == 'agent':
				return True
		except :
			return False	
	def has_module_permission(self, request):
		return True

class UserModuleRelAdmin(admin.ModelAdmin):
	form = ProductAdminForm

	list_display = ['user_full_name','module_name']
	def user_full_name(self,obj):
		# import pdb;pdb.set_trace()
		usr = User.objects.get(user_id=obj.user_id)
		return usr.user_full_name 
	def module_name(self,obj):
		mod = Module.objects.get(module_id=obj.module_id)
		return mod.module_name






from .forms import SchPackRelAdminForm
class SchoolerPackageRelAdmin(admin.ModelAdmin):
	# form = InsituteChangeForm

	list_display = ['package_name','institute_name'] #,'institute_email','institute_phone','institute_board'
	def package_name(self,obj):
		# import pdb;pdb.set_trace()
		schlr = SchoolerPackage.objects.get(schooler_package_id=obj.schooler_package_id)
		return schlr.package_name 
	def institute_name(self,obj):
		inst = Institute.objects.get(institute_id=obj.institute_id)
		return inst.institute_name
	# def institute_email(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	return inst.institute_email
	# def institute_phone(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	return inst.institute_phone
	# def institute_board(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	board_id = inst.institute_board_id
	# 	board = InstituteBoard.objects.get(institute_board_id=inst.institute_board_id)
	# 	return board.institute_board_name


		# # class StaffAdmin(admin.ModelAdmin):
		# # def get_queryset(self, request):
		# # pdb.set_trace()
		# # qs = super(StaffAdmin, self).get_queryset(request)
		# # if request.user.is_superuser:
		# # return qs

# import request
class SchoolerPackageAdmin(admin.ModelAdmin):
	# form = SchoolerPackageAdminForm
	list_display = [ 'package_name']   #


	def package_name(self,obj):
		schlr = SchoolerPackage.objects.get(schooler_package_id = obj.schooler_package_id)
		return schlr.package_name
	# def institute_name(self,obj):
	# 	import pdb;pdb.set_trace()
	# 	scrl = SchoolerPackageRel.objects.filter(schooler_package_id=obj.schooler_package_id)
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	return inst.institute_name 



# from .forms import SchPackRelAdminForm
# class SchoolerPackageRelAdmin(admin.ModelAdmin):
# 	form = SchPackRelAdminForm

# 	list_display = ['schooler_package_id','institute_name']
# 	# def get_queryset(self,request):
# 		# ab = super(SchoolerPackageRelAdmin,self).get_queryset(request)
# 		# if request.user.is_superuser:
# 			# return ab




# 	def package_name(self,obj):
# 		# import pdb;pdb.set_trace()
# 		schlr = SchoolerPackage.objects.get(schooler_package_id=obj.schooler_package_id)
# 		return schlr.schooler_package_id
# 	def institute_name(self,obj):
# 		# import pdb;pdb.set_trace()
# 		# srel = SchoolerPackageRel.objects.get(schooler_package_id=obj.schooler_package_id)

# 		inst = Institute.objects.get(institute_id=obj.institute_id)
# 		return inst.institute_name
	# def institute_email(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	return inst.institute_email
	# def institute_phone(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	return inst.institute_phone
	# def institute_board(self,obj):
	# 	inst = Institute.objects.get(institute_id=obj.institute_id)
	# 	board_id = inst.institute_board_id
	# 	board = InstituteBoard.objects.get(institute_board_id=inst.institute_board_id)
	# 	return board.institute_board_name
##______________________________this is for schlr of ids & package id displaying purpose_______________________________
# class SchoolerPackageRelAdmin(admin.ModelAdmin):
# 	list_display = ['schooler_package_id','institute_id']
# 	actions = ['Activate']


# 	list_per_page =5
# 	def Activate(self,request, queryset):
# 		status = list(queryset.values_list('institute_id','schooler_package_id'))
# 		# inst = queryset.model.objects.get(institute_id=state[0])
# 		for state in status:
# 			if state[1] == False:
# 				# send_email(state[2])
# 				inst = queryset.model.objects.get(institute_id=state[0])
# 				inst.institute_id=True
# 				inst.save()
# 				# qs= queryset
# 				# self.make_published(self,request,queryset)
# 				message = 'pass'
# 			else:
# 				message = 'fail'
# 	Activate.short_description = 'Activate'



#this is--------------------------------------------



class SchoolerUserRelAdmin(admin.ModelAdmin):
	form = SchlrUserRelAdminForm

	
	list_display = ['user_name',
	#linkify(field_name="login_type")
	'login_type_name'
	]
	def user_name(self,obj):
		# import pdb;pdb.set_trace()
		usr = User.objects.get(user_id=obj.user_id)
		return usr.user_full_name 
	def login_type_name(self,obj):
		login_type = LoginType.objects.get(login_type_id=obj.login_type_id)
		return login_type.login_type_name


class StudentAdmin(admin.ModelAdmin):
	list_display = ['student_id','student_name','student_email','institute_id']
	actions = ['Activate']
	list_per_page =5


	def Activate(self,request, queryset):
		status = list(queryset.values_list('student_id','student_name','student_email','institute_id'))
		# inst = queryset.model.objects.get(institute_id=state[0])
		for state in status:
			if state[1] == False:
				# send_email(state[2])
				inst = queryset.model.objects.get(institute_id=state[0])
				inst.institute_id=True
				inst.save()
				# qs= queryset
				# self.make_published(self,request,queryset)
				message = 'pass'
			else:
				message = 'fail'
	Activate.short_description = 'Activate'


class StaffAdmin(admin.ModelAdmin):
	list_display = ['staff_id','staff_name','staff_email','institute_id']
	actions = ['Activate']
	list_per_page = 5

	def Activate(self,request, queryset):
		status = list(queryset.values_list('staff_id','staff_name','staff_email','institute_id'))
		# inst = queryset.model.objects.get(institute_id=state[0])
		for state in status:
			if state[1] == False:
				# send_email(state[2])
				inst = queryset.model.objects.get(institute_id=state[0])
				inst.institute_id=True
				inst.save()
				# qs= queryset
				# self.make_published(self,request,queryset)
				message = 'pass'
			else:
				message = 'fail'
	Activate.short_description = 'Activate'


# class InstituteAdmins(admin.ModelAdmin):

# 	pass





# class CourseAdmin(admin.ModelAdmin):
#     list_display = ("name", "year", "view_students_link")
#
#     def view_students_link(self, obj):
#         count = obj.person_set.count()
#         url = (
#             reverse("admin:core_person_changelist")
#             + "?"
#             + urlencode({"courses__id": f"{obj.id}"})
#         )
#         return format_html('<a href="{}">{} Students</a>', url, count)
#
#     view_students_link.short_description = "Students"


admin.site.register(AdminUsers, CustomUserAdmin)

# Register your models here.
admin.site.register(User)
admin.site.register(Student,StudentAdmin)
admin.site.register(AdminRoles)
admin.site.register(Institute,InstituteAdmin)
admin.site.register(Module,ModuleAdmin)
admin.site.register(UserModuleRel,UserModuleRelAdmin)
admin.site.register(SchoolerPackageRel,SchoolerPackageRelAdmin)
admin.site.register(Staff,StaffAdmin)
# admin.site.register(SchoolerUserRel,SchoolerUserRelAdmin)
admin.site.register(LoginType)
admin.site.register(SchoolerPackage,SchoolerPackageAdmin)
# admin.site.register(Course,CourseAdmin)




# from django.utils.translation import ugettext_lazy 

# from django.contrib.admin import AdminSite

# AdminSite.site_title = ugettext_lazy('My Admin')

# AdminSite.site_header = ugettext_lazy('My Administration')

# AdminSite.index_title = ugettext_lazy('DATA BASE ADMINISTRATION')