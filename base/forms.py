from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import AdminUsers,UserModuleRel,User,SchoolerPackage,SchoolerPackageRel,SchoolerUserRel,Institute
from django import forms


class CustomUserCreationForm(UserCreationForm):

	class Meta(UserCreationForm):
		model = AdminUsers
		fields = ('email',)


class CustomUserChangeForm(UserChangeForm):

	class Meta:
		model = AdminUsers
		fields = ('email',)

class ProductAdminForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		
		# user = kwargs.pop('user')
		# self.logged_user = user/
		super(ProductAdminForm, self).__init__(*args, **kwargs)
		# import pdb;pdb.set_trace()
		usr = kwargs['instance']
		# self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id= usr.user_id).values_list("user_email", flat=True).distinct(),)
		self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id=usr.user_id).values_list("user_email", flat=True).distinct(),)
		# self.fields['module_id_id'] = forms.ModelChoiceField(queryset=Module.objects.get(module_id=usr.module_id_id).values_list("module_name",flat=True).distinct(),)
		# self.fields['address'] = forms.CharField(required=True)
		# self.fields['name'].widget.attrs ={'class': 'characters-remaining' ,
		#                                    'maxlength' :55 ,
		#                                    }
		# self.fields['short_description'].widget = forms.Textarea(attrs = {'class': 'characters-remaining',
		#                                                                    'maxlength' : 155,
		#                                                                    'rows' : 4
		#                                                                     })
		# self.fields['venuetypes'].widget.attrs = {'class': 'venue-type-select',}
		# self.fields['website'].widget = forms.TextInput()

	class Meta:
			model = UserModuleRel
			fields = ['user_id']
			# fields = [ 'name', 'address', 'email', 'mobile'  , 'venuetypes',  'logo' ,'short_description', 'long_description', 'website' ]

	# def clean_email(self):
	#     email =self.cleaned_data['email']
	#     if not email:
	#         email = self.logged_user.email
	#     return email

	# def clean(self):
	#     address_str = self.cleaned_data.pop('address')
	#     location = self.cleaned_data.pop('location')
	#     address = Address.objects.create(address1=address_str,
	#                            world_city=location,
	#                            city=location.city,
	#                            country=location.country)
	#     logged_user = self.logged_user
	#     contact, created = VenueContact.objects.get_or_create(auth_user=logged_user)
	#     self.cleaned_data.update({'address' : address, 'contact' : contact})


class PackageAdminForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):

		super(PackageAdminForm, self).__init__(*args, **kwargs)
		
		schlr = kwargs['instance']
		# self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id= usr.user_id).values_list("user_email", flat=True).distinct(),)
		self.fields['schooler_package_id'] =  forms.ModelChoiceField(label='Package name',queryset=SchoolerPackage.objects.filter(schooler_package_id=schlr.schooler_package_id).values_list("package_name", flat=True).distinct(),)
		

	class Meta:
			model = SchoolerPackageRel
			fields=[]

#=======================================================================================================#
class SchPackRelAdminForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(SchoolerPackageAdminForm, self).__init__(*args, **kwargs)
		# import pdb;pdb.set_trace()
		schlr = kwargs['instance']
		# self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id= usr.user_id).values_list("user_email", flat=True).distinct(),)
		self.fields['institute_id'] =  forms.ModelChoiceField(label='institute name',queryset=Institute.objects.filter(institute_id=schlr.institute_id).values_list("institute_name", flat=True).distinct(),)
		

	class Meta:
			model = SchoolerPackageRel
			fields = ['institute_id']

#======================================================================================================================#

class SchlrUserRelAdminForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):

		super(SchlrUserRelAdminForm, self).__init__(*args, **kwargs)
		usr = kwargs['instance']
		# self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id= usr.user_id).values_list("user_email", flat=True).distinct(),)
		# self.fields['package_id'] =  forms.ModelChoiceField(label='Package name',queryset=SchoolerPackage.objects.filter(schooler_package_id=schlr.schooler_package_id).values_list("package_name", flat=True).distinct(),)
		self.fields['user_id'] =  forms.ModelChoiceField(label='User Email',queryset=User.objects.filter(user_id=usr.user_id).values_list("user_email", flat=True).distinct(),)
		

	class Meta:
			model = SchoolerUserRel
			fields = ['user_id']


class InsituteChangeForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		
		super(InsituteChangeForm, self).__init__(*args, **kwargs)
		import pdb;pdb.set_trace()
		# package_name = SchoolerPackageRel,objects.get('package_name')
		self.fields['institute_name'] =  forms.CharField(max_length=150)

	# package_name = forms.CharField()

	# def save(self, commit=True):
	#     package_name = self.cleaned_data.get('package_name', None)
	#     # ...do something with extra_field here...
	#     return super(InstituteChangeForm, self).save(commit=commit)

	class Meta:
		model = Institute
		fields = ["institute_name"]