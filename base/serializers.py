
from django.contrib.auth.models import User
from base.models import Module,SchoolerPackageRel
from rest_framework import routers, serializers, viewsets
# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
        
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password']
        # fields = ['user_full_name', 'user_email', 'user_password', 'user_phone', 'user_dob', 'user_gender',
        # 'user_photo', 'user_reg_date', 'user_status', 'created_date', 'timestamp']

class AuthSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["username", "password"]

class ForgotPasswSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["email"]

class UpdatePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

class ChangePasswSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["email", "password"]
#from temp
# class ModuleSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Module
#         # fields = ['url', 'username', 'email', 'is_staff']
#         fields = ['module_name', 'module_isfree', 'module_price', 'module_url', 'module_image', 'partner_name',
#         'partner_email', 'partner_password', 'partner_phone', 'module_f_desc', 'module_status', 'created_date', 'timestamp']
class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        # fields = ['url', 'username', 'email', 'is_staff']
        fields = ['module_name']

# class ModuleCategorySerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = ModuleCategory
#         # fields = ['url', 'username', 'email', 'is_staff']
#         fields = ['module_id', 'module_cat_id']


# class UserModuleRelSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = UserModuleRel
#         # fields = ['url', 'username', 'email', 'is_staff']
#         fields = ['user_id', 'module_id']

# class UserValiditySerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = UserValidity
#         fields = ['user_id','created_time','expiry_time']
class SchoolerSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = SchoolerPackageRel
        fields = ['schooler_package_id','institute_id']
