from django.db import models

# Create your models here.
from django.db import models

import binascii
import os
from django.utils.translation import gettext_lazy as _


from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import UserManager
from django.conf import settings
from django.utils import timezone

# Create your models here.
class UserRole(models.Model):
	user_role_id = models.AutoField(primary_key=True)
	user_role_name = models.CharField(max_length=30,unique=True)
	user_role_f_desc = models.CharField(max_length=255)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed =False
		db_table = 'user_role'

	def __str__(self):
		return self.user_role_name

class Module(models.Model):
	module_id = models.AutoField(primary_key=True)
	module_name = models.CharField(max_length=30)
	module_isfree = models.BooleanField(default=False)
	module_price = models.FloatField()
	module_url = models.CharField(max_length=255)
	module_image = models.CharField(max_length=255)
	partner_name = models.CharField(max_length=30,blank=True)
	partner_email = models.CharField(max_length=70,blank=True)
	partner_password = models.CharField(max_length=50,blank=True)
	partner_phone = models.CharField(max_length=20,blank=True)
	module_f_desc = models.CharField(max_length=255)
	module_status = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		db_table = 'module'
		managed =False
		verbose_name = 'Partner'

	def __str__(self):
		return self.module_name

class InstituteBoard(models.Model):
	institute_board_id = models.AutoField(primary_key=True)
	institute_board_name = models.CharField(max_length=50)
	institute_board_f_desc = models.CharField(max_length=255)
	institute_board_status = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'institute_board'

	def __str__(self):
		return self.institute_board_name

class Institute(models.Model):
	institute_id = models.AutoField(primary_key=True)
	institute_type_id = models.PositiveIntegerField()
	institute_board_id = models.PositiveIntegerField()
	schooler_superintendent_id = models.PositiveIntegerField()
	institute_name = models.CharField(max_length=50,unique = True)
	institute_email = models.CharField(max_length=70)
	institute_phone = models.CharField(max_length=20,blank=True)
	institute_photo = models.CharField(max_length=200,blank=True)
	institute_addr = models.CharField(max_length=255,blank=True)
	institute_f_summary = models.CharField(max_length=255,blank=True)
	institute_estd = models.DateField()
	institute_class_count = models.PositiveIntegerField()
	institute_city = models.CharField(max_length=50,blank=True)
	institute_state = models.CharField(max_length=200,blank=True)
	institute_location = models.CharField(max_length=200,blank=True)
	institute_pincode = models.IntegerField()
	institute_status = models.BooleanField(default=False)
	created_date = models.DateField()
	timestamp = models.DateTimeField()
	institute_reg_code = models.CharField(max_length=30,blank=True)
	student_fee_term_count = models.PositiveIntegerField()

	class Meta:
		managed =False
		db_table = 'institute'
		verbose_name = 'Institute'
	
	def __str__(self):
		return self.institute_name


class AdminRoles(models.Model):
	user_role_name = models.CharField(max_length=30,unique=True)
	user_role_f_desc = models.CharField(max_length=255)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		verbose_name='Role'
		db_table = 'users_role'

	def __str__(self):
		return self.user_role_name


class User(models.Model):
	user_id = models.AutoField(primary_key=True)
	# user_role_id = models.ForeignKey(AdminRoles,to_field='user_role_name',on_delete=models.SET_NULL,null=True,blank=True)
	user_role_id = models.PositiveIntegerField(default=1003)
	user_full_name = models.CharField(max_length=70)
	user_email = models.CharField(max_length=70,unique=True)
	user_password = models.CharField(max_length=50)
	user_phone = models.CharField(max_length=20)
	user_dob = models.DateField()
	user_gender = models.CharField(max_length=1)
	user_photo = models.CharField(max_length=200)
	user_reg_date = models.DateTimeField()
	user_status = models.BooleanField(default=True)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed =False
		db_table = 'user'

	def __str__(self):
		return self.user_email

class AdminUsers(AbstractBaseUser,PermissionsMixin):
	# email = models.EmailField(max_length=254, unique=True)
	# roles = models.CharField(max_length=100,blank =True)
	admin_role_id = models.ForeignKey(AdminRoles,to_field='user_role_name',on_delete=models.SET_NULL,null=True,verbose_name='Role',blank=True)
	# admin_role_id = models.ManyToManyField(AdminRoles)
	email = models.ForeignKey(User,to_field='user_email',on_delete=models.SET_NULL,null =True,unique=True)
	
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	date_joined = models.DateTimeField(default=timezone.now)
	
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	objects = UserManager()
	
	class Meta:
		# unique_together = ('email', 'admin_role_id',)
		verbose_name = u'Admin'

	def __str__(self):
		return self.email.user_email

class Student(models.Model):
	student_id = models.AutoField(primary_key=True)
	institute_id = models.PositiveIntegerField()
	classroom_id = models.PositiveIntegerField()
	student_name = models.CharField(max_length=50)
	student_email = models.CharField(max_length=70)
	student_password = models.CharField(max_length=50)
	student_phone = models.CharField(max_length=20)
	student_photo = models.CharField(max_length=200)
	student_comments = models.CharField(max_length=255)
	student_rating = models.IntegerField()
	student_status = models.IntegerField()
	guardian_name = models.CharField(max_length=50)
	guardian_contact = models.CharField(max_length=20)
	roll_no = models.CharField(max_length=70)
	admission_no = models.CharField(max_length=70)
	has_live_access = models.IntegerField()
	has_raised = models.IntegerField()
	has_board = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'student'

class StudentFace(models.Model):
	student_id = models.PositiveIntegerField(primary_key=True)
	student_face_url = models.CharField(max_length=200)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'student_face'
		unique_together = (('student_id', 'student_face_url'),)


class StudentAttendance(models.Model):
	student_attendance_id = models.AutoField(primary_key=True)
	student_id = models.PositiveIntegerField()
	staff_id = models.PositiveIntegerField()
	subject_id = models.PositiveIntegerField()
	classroom_id = models.PositiveIntegerField()
	attendance_date = models.DateField()
	has_attended = models.IntegerField()
	has_attended_final = models.PositiveIntegerField()
	on_leave = models.PositiveIntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'student_attendance'
		unique_together = (('student_id', 'subject_id', 'attendance_date'),)

class ClassroomSnapshot(models.Model):
	classroom_snapshot_id = models.AutoField(primary_key=True)
	classroom_id = models.PositiveIntegerField()
	staff_id = models.PositiveIntegerField()
	subject_id = models.PositiveIntegerField()
	snapshot_url = models.CharField(max_length=512)
	rtsp_url = models.CharField(max_length=512)
	snapshot_status = models.CharField(max_length=50)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'classroom_snapshot'



class Notification(models.Model):
	notification_id = models.AutoField(primary_key=True)
	login_type_id = models.PositiveIntegerField()
	to_id = models.PositiveIntegerField()
	notif_title = models.CharField(max_length=100)
	notif_message = models.TextField()
	notif_for_url = models.TextField()
	notif_for = models.CharField(max_length=20)
	is_read = models.IntegerField()
	is_notified = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'notification'

class UserModuleRel(models.Model):
	user_id = models.PositiveIntegerField(primary_key=True)
	module_id = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'user_module_rel'
		# unique_together = (('user_id', 'module_id'),)


	def __str__(self):
		ret = '{0} Module for user {1}'
		mod = Module.objects.get(module_id=self.module_id)
		usr = User.objects.get(user_id=self.user_id)
		# return ret.format(mod.module_name, usr.user_email)
		return str(self.user_id)
# class UserModuleRel(models.Model):
#     user_id = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user',primary_key=True)
#     module_id_id = models.ForeignKey(Module,on_delete=models.CASCADE,related_name='module')
#     created_date = models.DateField()
#     timestamp = models.DateTimeField()

#     class Meta:
#         managed = False
#         db_table = 'user_module_rel'
#         unique_together = (('user_id', 'module_id_id'),)

	# def __str__(self):
	#  	ret = 'Module {0} for user {1}'
	#  	return ret.format(str(self.module_id_id), str(self.user_id))

class SchoolerPackage(models.Model):
	schooler_package_id = models.AutoField(primary_key=True)
	package_name = models.CharField(max_length = 50)

	created_date = models.DateField()
	timestamp = models.DateTimeField()
	class Meta:
		managed = False
		db_table = 'schooler_package'
		unique_together = (('schooler_package_id', 'package_name'))
	def __str__(self):
		return str(self.package_name)
		
	
class SchoolerPackageRel(models.Model):
	schooler_package_id = models.PositiveIntegerField()
	institute_id = models.PositiveIntegerField(primary_key = True)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		# managed = False
		db_table = 'schooler_package_rel'
		unique_together = (('schooler_package_id', 'institute_id'),)


	def __str__(self):
		# return str(self.institute_id)
		return '{} - {}'.format(self.schooler_package_id, self.institute_id)
#changes here


# class Institutes(Institute):
# 	customers = models.ManyToManyField(Institute)

# 	class Meta:
# 		proxy = True








class LoginType(models.Model):
	login_type_id = models.AutoField(primary_key=True)
	login_type_name = models.CharField(max_length=30)
	login_type_f_desc = models.CharField(max_length=255)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	def __str__(self):
		return self.login_type_name

	class Meta:
		managed = False
		db_table = 'login_type'


class SchoolerUserRel(models.Model):
	user_id = models.PositiveIntegerField(primary_key=True)
	login_type = models.ForeignKey(LoginType, on_delete=models.CASCADE)
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'schooler_user_rel'
		unique_together = (('user_id', 'login_type_id'),)
	def __str__(self):
		ret = '{0} Module for user {1}'
		login_type = LoginType.objects.get(login_type_id=self.login_type_id)
		usr = User.objects.get(user_id=self.user_id)
		# return ret.format(mod.module_name, usr.user_email)
		return str(self.user_id)




class InstituteModuleRel(models.Model):
	institute_id = models.PositiveIntegerField(primary_key=True)
	module_id = models.PositiveIntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'institute_module_rel'
		unique_together = (('institute_id', 'module_id'),)


# class Student(models.Model):
#     student_id = models.AutoField(primary_key=True)
#     institute_id = models.PositiveIntegerField()
#     classroom_id = models.PositiveIntegerField()
#     student_name = models.CharField(max_length=50)
#     student_email = models.CharField(max_length=70)
#     student_password = models.CharField(max_length=50)
#     student_phone = models.CharField(max_length=20)
#     student_photo = models.CharField(max_length=200)
#     student_comments = models.CharField(max_length=255)
#     student_rating = models.IntegerField()
#     student_status = models.IntegerField()
#     guardian_name = models.CharField(max_length=50)
#     guardian_contact = models.CharField(max_length=20)
#     roll_no = models.CharField(max_length=70)
#     admission_no = models.CharField(max_length=70)
#     has_live_access = models.IntegerField()
#     has_raised = models.IntegerField()
#     has_board = models.IntegerField()
#     created_date = models.DateField()
#     timestamp = models.DateTimeField()

#     class Meta:
#         managed = False
#         db_table = 'student'
#     def __str__(self):
#     	return self.student_name


class Staff(models.Model):
	staff_id = models.AutoField(primary_key=True)
	institute_id = models.PositiveIntegerField()
	staff_name = models.CharField(max_length=50)
	staff_email = models.CharField(max_length=70)
	staff_password = models.CharField(max_length=50)
	staff_pin = models.SmallIntegerField()
	staff_phone = models.CharField(max_length=20)
	staff_photo = models.CharField(max_length=200)
	staff_expertise_in = models.CharField(max_length=80)
	staff_comments = models.CharField(max_length=255)
	staff_rating = models.PositiveIntegerField()
	staff_status = models.IntegerField()
	created_date = models.DateField()
	timestamp = models.DateTimeField()

	class Meta:
		managed = False
		db_table = 'staff'

	def __str__(self):
		return self.staff_name





# class Course(models.Model):
#     name = models.TextField()
#     year = models.IntegerField()
#
#     class Meta:
#         unique_together = ("name", "year", )