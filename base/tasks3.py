# from celery import shared_task
import os
import datetime
from datetime import date

import cv2
# The idea of this code is to verify is the subject present in the query image
# is present in the session image.
#
# D. Mery, UC, October, 2018
# http://dmery.ing.puc.cl
# from kgtopg.settings import BASE_DIR
# from celery.contrib import rdb
import requests
import shutil  # to save it locally
from utils2 import print_definitions, imreadx, face_descriptor, load_fr_model, face_detection, face_scores
from utils2 import session_descriptors, is_face, show_face

# from base.models import Student,StudentFace,StudentAttendance,ClassroomSnapshot,Notification

import dlib
from PIL import Image
from skimage import io
import numpy as np
# definitions
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="depl",
  password="Depl@0369",
	database="kgtopg"
)


mycursor = mydb.cursor()
fd_method = 0               # face detection method (0:HOG, 1: CNN)
# face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
fr_method = 1
sc_method = 0               # 0 cosine similarity, 1 euclidean distance
uninorm = 1               # 1 means descriptor has norm = 1
theta = 0.6             # threshold for cosine similarity
print_scr = 1               # print scores
show_img = 0               # show images
print("[facer] : ----------------------------------------------------------------")
print_definitions(fd_method, fr_method, sc_method, uninorm, theta)

# fr_model = fr_model_global
# query image: read, display and description
# print("[facer] : reading query image " + "...")
D = None
print("[facer] : loading face recognition model...")
fr_model = load_fr_model(fr_method)
# init
# fr_model_global = None

# def load_fr_model_global(fr_method):
# 	fr_model_global = load_fr_model(fr_method)
# 	return fr_model_global


def detect_face(image):

	# Create a face detector
	face_detector = dlib.get_frontal_face_detector()

	# Run detector and get bounding boxes of the faces on image.
	detected_faces = face_detector(image, 1)
	face_frames = [(x.left(), x.top(), x.right(), x.bottom())
	                for x in detected_faces]

	# face_frames = [(x.top(),x.right(), x.bottom(),x.left()) for x in detected_faces]

	return face_frames


def face_attend(stud_id, instituteId, staffId, subjectId, classroomId, image_url, filename, class_img_path):
	global fr_method
	global D
	global fd_method
	global uninorm
	global fr_model

	detected = False
	r = requests.get(image_url, stream=True)
	if r.status_code == 200:
		# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
		r.raw.decode_content = True
		# Open a local file with wb ( write binary ) permission.
	#	tmp_path = "/home//vijay/prog/python/kgtopg-platform/temp/"
		tmp_path = "/tmp/"
		with open(tmp_path+filename, 'wb') as f:
			shutil.copyfileobj(r.raw, f)
		print('Image sucessfully Downloaded: ', filename)
	# img_query   = "/home/vijay/prog/python/kgtopg-platform/base/query6a.jpg"   # query file image (one cropped face)
	img_query = tmp_path+filename
	# img_session = os.path.join(BASE_DIR,"base/test2.jpg") # session file image (picture of the classroom with students)

	# load deep learning model (if any)
	# fr_model = load_fr_model(fr_method)
	desc_q = None
	# face_q    = imreadx(img_query,show_img)
	image = io.imread(img_query)
	# image = cv2.cvtColor(image_q,cv2.COLOR_BGR2GRAY)
	detected_faces = detect_face(image)
	for n, face_rect in enumerate(detected_faces):
		face = Image.fromarray(image).crop(face_rect)  # facecrop
		im = np.array(face)
		desc_q = face_descriptor(im, fr_method, fr_model, uninorm)
	# desc_q = face_descriptor(image,fr_method,fr_model,uninorm)
	# session image: read, display and face detection

	# computation of scores between query face and every face of the session
	t = face_scores(D, desc_q, sc_method, print_scr)

	# find if query face is in session image
	i = is_face(t, theta, sc_method)

	print('Now we are saving attendance..')
	today = date.today().isoformat()
	SQL4 = None
	data = None
	if i >= 0:
		# show_face(S,faces[i],i,show_img)
		print("[facer] : face #" + str(i) +
		      " was detected with score = " + str(t[i]))
		SQL4 = " UPDATE student_attendance SET has_attended_final = 1 WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND attendance_date=%s "

		# std_att_inst.has_attended_final = 1
		# std_att_inst.save()
		detected = True
	else:
		print("[facer] : face in query image not detected in session image")
		# std_att_inst.has_attended_final = 0
		# std_att_inst.save()
		SQL4 = " UPDATE student_attendance SET has_attended_final = 0 WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND attendance_date=%s "
		# data = (stud_id, staffId, subjectId, classroomId, today)

	try:
		data = (stud_id, staffId, subjectId, classroomId, today)

		mycursor.execute(SQL4, data)
	except:
		print("Could not save attendance for student %s ", stud_id)
	return detected


def face_attend2(fc, class_img_path):
	global fr_method
	global D
	global fd_method
	global uninorm
	global fr_model

	detected = False

	img_query = fc
	# img_session = os.path.join(BASE_DIR,"base/test2.jpg") # session file image (picture of the classroom with students)

	# load deep learning model (if any)
	# fr_model = load_fr_model(fr_method)
	desc_q = None
	# face_q    = imreadx(img_query,show_img)
	image = io.imread(img_query)
	# image = cv2.cvtColor(image_q,cv2.COLOR_BGR2GRAY)
	detected_faces = detect_face(image)
	for n, face_rect in enumerate(detected_faces):
		face = Image.fromarray(image).crop(face_rect)  # facecrop
		im = np.array(face)
		desc_q = face_descriptor(im, fr_method, fr_model, uninorm)
	# desc_q = face_descriptor(image,fr_method,fr_model,uninorm)
	# session image: read, display and face detection

	# computation of scores between query face and every face of the session
	t = face_scores(D, desc_q, sc_method, print_scr)

	# find if query face is in session image
	i = is_face(t, theta, sc_method)

	print("Face detection complete for face..")
	# print('Now we are saving attendance..')
	# today = date.today().isoformat()
	# SQL4 = None
	# data = None
	# if i >= 0:
	# 	# show_face(S,faces[i],i,show_img)
	# 	print("[facer] : face #" + str(i) +
	# 	      " was detected with score = " + str(t[i]))
	# 	SQL4 = " UPDATE student_attendance SET has_attended_final = 1 WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND attendance_date=%s "

	# 	# std_att_inst.has_attended_final = 1
	# 	# std_att_inst.save()
	# 	detected = True
	# else:
	# 	print("[facer] : face in query image not detected in session image")
	# 	# std_att_inst.has_attended_final = 0
	# 	# std_att_inst.save()
	# 	SQL4 = " UPDATE student_attendance SET has_attended_final = 0 WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND attendance_date=%s "
	# 	# data = (stud_id, staffId, subjectId, classroomId, today)

	# try:
	# 	data = (stud_id, staffId, subjectId, classroomId, today)

	# 	mycursor.execute(SQL4, data)
	# except:
	# 	print("Could not save attendance for student %s ", stud_id)
	return detected


def make_thumbnails(instituteId, staffId, subjectId, classroomId):
	# load global face recognition model
	# fr_model = load_fr_model_global(1)

	# rdb.set_trace()
	# Get all students of the particular institute/classroom/staff/subject

	# fr_model_global = load_fr_model(fr_method)

	dataset = [
		[
			'/home/vijay/faces/9.jpeg',
			'/home/vijay/faces/10.jpeg',
			'/home/vijay/faces/11.jpeg'
		],
		[
			'/home/vijay/faces/12.jpeg',
			'/home/vijay/faces/13.jpeg',
			'/home/vijay/faces/14.jpeg'
		],
		[
			'/home/vijay/faces/16.jpeg',
			'/home/vijay/faces/17.jpeg',
			'/home/vijay/faces/18.jpeg'
		],
		[
			'/home/vijay/faces/1.png',
			'/home/vijay/faces/2.png',
			'/home/vijay/faces/3.png'
		],
	]

	global D
	global fr_model
	SQL = " SELECT student_id FROM student WHERE institute_id=%s AND classroom_id=%s "
	data = (instituteId, classroomId)

	mycursor.execute(SQL, data)
	myresult = mycursor.fetchall()

	# print(mydb)

	# std_id = Student.objects.filter(institute_id=instituteId,classroom_id=classroomId).values_list('student_id')

	# image_url = StudentFace.objects.get(student_id=std_id).values('student_face_url')
	# image_url = "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTE5NDg0MDU0OTM2NTg1NzQz/tom-cruise-9262645-1-402.jpg"
	# print(std_id)
	#  rdb.set_trace()
	today = date.today().isoformat()  
	
	# Take classroom snapshot url and create file from it
	# class_img_session = ClassroomSnapshot.objects.filter(classroom_id=classroomId,created_date=today).order_by('-timestamp')[0]
	# class_image_url = class_img_session.snapshot_url
	# class_rtsp_url = class_img_session.rtsp_url
	print("Start  VideoCapture")
	#----------------local testing url-----------------#
	class_rtsp_url = "rtsp://admin:MOJIOD@192.168.10.100"
	#--------------------------------------------------#
	stream = cv2.VideoCapture(class_rtsp_url)
	r, f = stream.read()
	# gray = cv2.cvtColor(f,cv2.COLOR_BGR2GRAY)
	cv2.imwrite("/tmp/opencv1.jpg", f)
	stream.release()
	print("end  VideoCapture")
	tmp_path = "/tmp/"
	class_img_filename = "opencv1.jpg"
	class_img_path = tmp_path+class_img_filename
	img_session = class_img_path

	print("reading session image")
	S     = imreadx(img_session,show_img)
	print("[facer] : detecting faces in session image " + img_session + "...")
	
	faces  = face_detection(S,fd_method)
	
	print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

	# computation of descriptors in detected faces of session image
	print("[facer] : finding "  + " in session image ...")
	D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

	# class_img_filename = class_img_session.snapshot_url.split("?")[0].split("/")[-1]
	# r = requests.get(class_image_url, stream = True)
	# if r.status_code == 200:
	# 	# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
	# 	r.raw.decode_content = True
	# 	# Open a local file with wb ( write binary ) permission.
	# 	tmp_path = "/tmp/"
	# 	with open(tmp_path+class_img_filename,'wb') as f:
	# 		shutil.copyfileobj(r.raw, f)
		# print('Image sucessfully Downloaded: ',class_img_filename)

	# classroom snapshot FILE now available..
	print('Classroom snapshot file now available..');

	# print("[facer] : loading face recognition model...") 
	# fr_model = load_fr_model(fr_method) 

	# filename = sf.student_face_url.split("?")[0].split("/")[-1] 

	# Iterating each student..
	# for index in range(std_id.__len__()):

	for studnt in myresult:
	  # print(x)
		stud_id = studnt[0]
		try:
			# studnt = std_id[index]
			# studnt = result[index]
			# rdb.set_trace()
			# attendance query
			
			SQL2 = " SELECT * FROM student_attendance WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND created_date=%s" # (YYYY-mm-dd)
			# SQL2 = " SELECT * FROM student_attendance WHERE student_id={std} AND staff_id={stf} AND subject_id={sub} AND classroom_id={cla} AND created_date={day}".format(std=studnt[0],stf=staffId,sub=subjectId,cla=classroomId,day=today)
			data = (stud_id, staffId, subjectId, classroomId, today)
			# std_att_inst = StudentAttendance.objects.get(student_id=std_id[index][0],staff_id=staffId,
			# subject_id=subjectId,classroom_id=classroomId,created_date=today)

			mycursor.execute(SQL2,data)
			std_attd_inst = mycursor.fetchall()

			if not std_attd_inst:
				print("Attendance records not found for current day for student %s..", stud_id)
				print("Now saving attendance..")


				# write INSERT SQL
				# std_attd_inst.save()
				SQL4 = " INSERT INTO student_attendance(student_id,staff_id,subject_id,classroom_id,attendance_date,has_attended,has_attended_final,on_leave,created_date,timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
				data = (stud_id, staffId, subjectId, classroomId, today, 0, 0, 0, today, datetime.datetime.now())

				mycursor.execute(SQL4,data)

				# now fetch and save in our variable

				SQL2 = " SELECT * FROM student_attendance WHERE student_id=%s AND staff_id=%s AND subject_id=%s AND classroom_id=%s AND created_date=%s" # (YYYY-mm-dd)
				# SQL2 = " SELECT * FROM student_attendance WHERE student_id={std} AND staff_id={stf} AND subject_id={sub} AND classroom_id={cla} AND created_date={day}".format(std=studnt[0],stf=staffId,sub=subjectId,cla=classroomId,day=today)
				data = (stud_id, staffId, subjectId, classroomId, today)
				# std_att_inst = StudentAttendance.objects.get(student_id=std_id[index][0],staff_id=staffId,
				# subject_id=subjectId,classroom_id=classroomId,created_date=today)

				mycursor.execute(SQL2,data)
				std_attd_inst = mycursor.fetchall()

			# else:

			# print(std_att_inst)

			# check if already record exists with attended status..
			if std_attd_inst[0][6] == 1:
				pass # should not be passed????
			else:
				# perform face recognization
				pass

			dataset2 = [
					'/home/vijay/faces/9.jpeg',
					'/home/vijay/faces/10.jpeg',
					'/home/vijay/faces/11.jpeg'
				]

			for dt in dataset2:
				detected = face_attend2(dt,class_img_path)
			
			# if std_att_inst.has_attended== False:
			# 	# image_url = StudentFace.objects.filter(student_id=std_id[index][0]).latest().values('student_face_url')
				# try:
				# 	SQL3 = " SELECT * FROM student_face WHERE student_id = %s "
				# 	# sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
					
				# 	# stu_faces = StudentFace.objects.filter(student_id=studnt[0])
				# 	data = (stud_id,)
				# 	mycursor.execute(SQL3,data)

				# 	stu_faces = mycursor.fetchall()
				# 	for sf in stu_faces:#range(stu_faces.__len__()) :
				# 		# reassign
				# 		# sf = stu_faces[s_index]
				# 		image_url = sf[2] #sf.student_face_url
				# 		filename = image_url.split("?")[0].split("/")[-1] #sf.student_face_url.split("?")[0].split("/")[-1]
				# 		detected = face_attend(stud_id,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path)
				# 		if detected:
				# 			break
				# 		# else:
				# 		# 	break

				# 	# if(!detected):
				# 	# 	SQL4 = " INSERT INTO student_attendance(student_id,staff_id,subject_id,classroom_id,attendance_date,has_attended,has_attended_final,on_leave,created_date,timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
				# 	# 	data = (stud_id, staffId, subjectId, classroomId, today, 0, 0, 0, today, datetime.datetime.now())

				# 	# 	mycursor.execute(SQL4,data)

				# except Exception as e:
				# 	print('exception reading student faces %s ', e)
				# 	# std_attd_inst[0].has_attended_final = 0

				# 	# write INSERT SQL
				# 	# std_attd_inst.save()
				# 	# SQL4 = " INSERT INTO student_attendance(student_id,staff_id,subject_id,classroom_id,attendance_date,has_attended,has_attended_final,on_leave,created_date,timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
				# 	# data = (stud_id, staffId, subjectId, classroomId, today, 0, 0, 0, today, datetime.datetime.now())

				# 	# mycursor.execute(SQL4,data)
				# 	continue

			# else:
			# 	continue
		except Exception as e:
			print('Exception in getting attendance record %s', e)
			# std_att_inst = StudentAttendance.objects.create(student_id=std_id[index][0],staff_id=staffId,subject_id=subjectId,classroom_id=classroomId,created_date=today,attendance_date=today,has_attended=0,has_attended_final=0,on_leave=0,timestamp=datetime.datetime.now())
			# # sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]

			# SQL4 = " INSERT INTO student_attendance(student_id,staff_id,subject_id,classroom_id,attendance_date,has_attended,has_attended_final,on_leave,created_date,timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
			# data = (stud_id, staffId, subjectId, classroomId, today, 0, 0, 0, today, datetime.datetime.now())

			# # image_url = sf.student_face_url
			# # filename = sf.student_face_url.split("?")[0].split("/")[-1]
			# # face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename)
			# try:
			# 	SQL3 = " SELECT * FROM student_face WHERE student_id = %s "
			# 	# sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
				
			# 	# stu_faces = StudentFace.objects.filter(student_id=studnt[0])
			# 	data = (stud_id,)
			# 	mycursor.execute(SQL3,data)

			# 	stu_faces = mycursor.fetchall()
			# 	for sf in stu_faces:#range(stu_faces.__len__()) :
			# 		# reassign
			# 		# sf = stu_faces[s_index]
			# 		image_url = sf[2] #sf.student_face_url
			# 		filename = image_url.split("?")[0].split("/")[-1] #sf.student_face_url.split("?")[0].split("/")[-1]
			# 		detected = face_attend(stud_id,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path)
			# 		if detected:
			# 			break
			# 		# else:
			# 		# 	break

			# except :
			# 	# std_attd_inst[0].has_attended_final = 0

			# 	# write INSERT SQL
			# 	# std_attd_inst.save()
			# 	SQL4 = " INSERT INTO student_attendance(student_id,staff_id,subject_id,classroom_id,attendance_date,has_attended,has_attended_final,on_leave,created_date,timestamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
			# 	data = (stud_id, staffId, subjectId, classroomId, today, 0, 0, 0, today, datetime.datetime.now())

			# 	mycursor.execute(SQL4,data)
			# 	continue


			
			# if std_att_inst.has_attended== False:
			# 	try:
			# 		sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
			# 		image_url = sf.student_face_url
			# 		filename = sf.student_face_url.split("?")[0].split("/")[-1]
			# 	except:
			# 		pass
			# else:
			# 	continue
		# filename = image_url.split("/")[-1]

	# save and generate notification to staff
	
	try:
		notif_title = "Attendance Complete"
		notif_msg = "Attendance for your class is completed. Please check."
		notif_url = "teacher-classroom-attendance?id={0}&subid={1}".format(classroomId, subjectId)
		print("notification url is %s ", notif_url)
		# notif= Notification.objects.create(from_id=0,login_type_id = 102 ,to_id = staffId ,notif_title = "Attendance Complete" , notif_message ="Attendance for your class is completed. Please check.", notif_for_url = notif_url, notif_for = "attendance",is_read = 0,is_notified = 0,created_date = today,timestamp = datetime.datetime.now())
		SQL = " INSERT INTO notification(login_type_id, from_id, to_id, notif_title, notif_message, notif_for_url, notif_for, is_read, is_notified, created_date, timestamp) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') "
		data = (102, 0, staffId, notif_title, notif_msg, notif_url, "attendance", 0, 1, today, datetime.datetime.now())
	except Exception as e:
		print("notification cannot be saved %s ", e)


make_thumbnails(1061, 234, 124, 284)
