# from celery import shared_task
# import os
# import datetime
# from datetime import date
# import time
# import  cv2
# # The idea of this code is to verify is the subject present in the query image 
# # is present in the session image.
# #
# # D. Mery, UC, October, 2018
# # http://dmery.ing.puc.cl
# from kgtopg.settings import BASE_DIR
# from celery.contrib import rdb
# import requests
# import shutil # to save it locally
# from .utils import print_definitions, imreadx, face_descriptor, load_fr_model, face_detection, face_scores
# from .utils import session_descriptors, is_face, show_face

# from base.models import Student,StudentFace,StudentAttendance,ClassroomSnapshot,Notification

# import dlib
# from PIL import Image
# from skimage import io
# import numpy as np
# # definitions

# # init
# # fr_model_global = None

# # def load_fr_model_global(fr_method):
# # 	fr_model_global = load_fr_model(fr_method)

# def detect_face(image):

# 	# Create a face detector
# 	face_detector = dlib.get_frontal_face_detector()

# 	# Run detector and get bounding boxes of the faces on image.
# 	detected_faces = face_detector(image, 1)
# 	face_frames = [(x.left(), x.top(),
# 					x.right(), x.bottom()) for x in detected_faces]

# 	return face_frames

# def face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D):
# 	detected = False
# 	tmp_path = "/tmp/"
# 	#---------------------Getting student url---------------------------------------------------------#
# 	r = requests.get(image_url, stream = True)
# 	try:
# 		if r.status_code == 200:
# 			# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
# 			r.raw.decode_content = True
# 			# Open a local file with wb ( write binary ) permission.
# 			with open(tmp_path+filename,'wb') as f:
# 				shutil.copyfileobj(r.raw, f)
# 			print('Image sucessfully Downloaded: ',filename)
# 	except Exception as e:
# 		print(e)
# 	# img_query   = "/home/vijay/prog/python/kgtopg-platform/base/query6a.jpg"   # query file image (one cropped face)
# 	img_query = tmp_path+filename
# 	# img_session = class_img_path
# 	#------------------old---------------------------------------------------------------------------#
# 	# img_session = os.path.join(BASE_DIR,"base/test2.jpg") # session file image (picture of the classroom with students)
# 	# fd_method   = 0               # face detection method (0:HOG, 1: CNN)
# 	# fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
# 	# sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
# 	# uninorm     = 1               # 1 means descriptor has norm = 1
# 	# theta       = 0.6             # threshold for cosine similarity
# 	# print_scr   = 1               # print scores
# 	# show_img    = 0               # show images
# 	# print_definitions(fd_method,fr_method,sc_method,uninorm,theta)

# 	# # load deep learning model (if any)
# 	# print("[facer] : loading face recognition model...") 
# 	# fr_model = load_fr_model(fr_method)
# 	# fr_model = fr_model_global
# 	# query image: read, display and description
# 	#-------------------------------------------------------------------------------------------------#
# 	print("[facer] : reading query image " + "...")
# 	try:
# 		# face_q    = imreadx(img_query,show_img)
# 		image = io.imread(img_query)
# 		# image = cv2.cvtColor(image_q,cv2.COLOR_BGR2GRAY)
# 		detected_faces = detect_face(image)
# 		for n, face_rect in enumerate(detected_faces):
# 			face = Image.fromarray(image).crop(face_rect)  #facecrop
# 			im = np.array(face)
# 			desc_q = face_descriptor(im,fr_method,fr_model,uninorm)
# 	except Exception as e:
# 		print(e)
# 	# session image: read, display and face detection
# 	# S     = imreadx(img_session,show_img)
# 	# print("[facer] : detecting faces in session image " + img_session + "...") 
# 	# faces  = face_detection(S,fd_method)
# 	# print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

# 	# # computation of descriptors in detected faces of session image
# 	# print("[facer] : finding "  + " in session image ...")
# 	# D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

# 	# computation of scores between query face and every face of the session
# 	try:
# 		t = face_scores(D,desc_q,sc_method,print_scr)
# 	except Exception as e:
# 		print(e)

# 	# find if query face is in session image
# 	try:
# 		i = is_face(t,theta,sc_method)
# 	except Exception as e:
# 		print(e)

# 	if i>=0:
# 		print("[facer] : face #" + str(i) + " was detected with score = " + str(t[i]))
# 		std_att_inst.has_attended_final = 1
# 		std_att_inst.save()
# 		detected = True
# 	else:
# 		print("[facer] : face in query image not detected in session image")
# 		std_att_inst.has_attended_final = 0
# 		std_att_inst.save()

# 	return detected

# def face_attend2(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D):

# 	detected = False
# 	tmp_path = "/tmp/"
# 	#---------------------Getting student url---------------------------------------------------------#
# 	r = requests.get(image_url, stream = True)
# 	if r.status_code == 200:
# 		# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
# 		r.raw.decode_content = True
# 		# Open a local file with wb ( write binary ) permission.
# 		with open(tmp_path+filename,'wb') as f:
# 			shutil.copyfileobj(r.raw, f)
# 		print('Image sucessfully Downloaded: ',filename)
# 	# img_query   = "/home/vijay/prog/python/kgtopg-platform/base/query6a.jpg"   # query file image (one cropped face)
# 	img_query = tmp_path+filename

# 	# img_query = image_url
# 	# img_session = class_img_path
# 	#------------------old---------------------------------------------------------------------------#
# 	# img_session = os.path.join(BASE_DIR,"base/test2.jpg") # session file image (picture of the classroom with students)
# 	# fd_method   = 0               # face detection method (0:HOG, 1: CNN)
# 	# fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
# 	# sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
# 	# uninorm     = 1               # 1 means descriptor has norm = 1
# 	# theta       = 0.6             # threshold for cosine similarity
# 	# print_scr   = 1               # print scores
# 	# show_img    = 0               # show images
# 	# print_definitions(fd_method,fr_method,sc_method,uninorm,theta)

# 	# # load deep learning model (if any)
# 	# print("[facer] : loading face recognition model...") 
# 	# fr_model = load_fr_model(fr_method)
# 	# fr_model = fr_model_global
# 	# query image: read, display and description
# 	#-------------------------------------------------------------------------------------------------#
# 	# print("[facer] : reading query image '" + img_query + "'...")
	
# 	# face_q    = imreadx(img_query,show_img)
# 	image = io.imread(img_query)
# 	desc_q = None

# 	# image = cv2.cvtColor(image_q,cv2.COLOR_BGR2GRAY)
# 	detected_faces = detect_face(image)
# 	for n, face_rect in enumerate(detected_faces):
# 		face = Image.fromarray(image).crop(face_rect)  #facecrop
# 		im = np.array(face)
# 		desc_q = face_descriptor(im,fr_method,fr_model,uninorm)

# 	# session image: read, display and face detection
# 	# S     = imreadx(img_session,show_img)
# 	# print("[facer] : detecting faces in session image " + img_session + "...") 
# 	# faces  = face_detection(S,fd_method)
# 	# print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

# 	# # computation of descriptors in detected faces of session image
# 	print("[facer] : finding "  + " in session image ...")
# 	# D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

# 	# computation of scores between query face and every face of the session
# 	t = face_scores(D,desc_q,sc_method,print_scr)

# 	# find if query face is in session image
# 	i = is_face(t,theta,sc_method)

# 	if i>=0:
# 		print("[facer] : face #" + str(i) + " was detected with score = " + str(t[i]))
# 		std_att_inst.has_attended_final = 1
# 		std_att_inst.save()
# 		detected = True
# 	else:
# 		print("[facer] : face in query image not detected in session image")
# 		std_att_inst.has_attended_final = 0
# 		std_att_inst.save()

# 	return detected

# @shared_task

# def make_thumbnails(instituteId, staffId,subjectId,classroomId):
# 	fd_method   = 0               # face detection method (0:HOG, 1: CNN)
# 	fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
# 	sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
# 	uninorm     = 1               # 1 means descriptor has norm = 1
# 	theta       = 0.6             # threshold for cosine similarity
# 	print_scr   = 1               # print scores
# 	show_img    = 0               # show images
	
# 	print("[facer] : ----------------------------------------------------------------") 
# 	# print_definitions(fd_method,fr_method,sc_method,uninorm,theta)
# 	print("[facer] : loading face recognition model...") 
	
# 	fr_model = load_fr_model(fr_method) 

# 	std_id = Student.objects.filter(institute_id=instituteId,classroom_id=classroomId).values_list('student_id')

# 	# image_url = StudentFace.objects.get(student_id=std_id).values('student_face_url')
# 	# image_url = "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTE5NDg0MDU0OTM2NTg1NzQz/tom-cruise-9262645-1-402.jpg"
# 	# print(std_id)
# 	#  rdb.set_trace()
# 	today = date.today()
	
# 	# Take classroom snapshot url and create file from it
# 	# class_img_session = ClassroomSnapshot.objects.filter(classroom_id=classroomId,created_date=today).order_by('-timestamp')[0]
# 	# class_image_url = class_img_session.snapshot_url
# 	# class_rtsp_url = class_img_session.rtsp_url
# 	#----------------local testing url-----------------#
# 	class_rtsp_url = "rtsp://admin:MOJIOD@192.168.10.100"
# 	#--------------------------------------------------#
# 	stream = cv2.VideoCapture(class_rtsp_url)
# 	r, f = stream.read()
# 	# gray = cv2.cvtColor(f,cv2.COLOR_BGR2GRAY)
# 	cv2.imwrite("/tmp/opencv1.jpg", f)
# 	stream.release()
# 	#---------------------------Class  Room Snapshot------------------------------------#
# 		# class_img_filename = class_img_session.snapshot_url.split("?")[0].split("/")[-1]
# 	# r = requests.get(class_image_url, stream = True)
# 	# if r.status_code == 200:
# 	# 	# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
# 	# 	r.raw.decode_content = True
# 	# 	# Open a local file with wb ( write binary ) permission.
# 	# 	tmp_path = "/tmp/"
# 	# 	with open(tmp_path+class_img_filename,'wb') as f:
# 	# 		shutil.copyfileobj(r.raw, f)
# 		# print('Image sucessfully Downloaded: ',class_img_filename)
# 	#------------------------------------------------------------------------------------#
# 	tmp_path = "/tmp/"
# 	class_img_filename = "opencv1.jpg" #need to change to dynamic name
# 	class_img_path = tmp_path+class_img_filename
# 	img_session = class_img_path

# 	S     = imreadx(img_session,show_img)
	
# 	print("[facer] : detecting faces in session image " + img_session + "...")
	
# 	t0 = time.time()

# 	print("before face detection")
# 	faces  = face_detection(S,fd_method)

# 	print(time.time() - t0)
# 	print("after face detection")
	
# 	print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

# 	# computation of descriptors in detected faces of session image
# 	print("[facer] : finding "  + " in session image ...")
	
# 	t0 = time.time()
# 	print("before session descriptors")
# 	D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

# 	print(time.time() - t0)
# 	print("after session descriptors")

# 	#--------------------------- Iterating each student..--------------------------------------#
# 	for index in range(std_id.__len__()):
# 		try:
# 			studnt = std_id[index]
# 			# attendance query
# 			std_att_inst = StudentAttendance.objects.get(student_id=std_id[index][0],staff_id=staffId,
# 				subject_id=subjectId,classroom_id=classroomId,created_date=today)
# 			# print(std_att_inst)

# 			# check if already record exists with attended status..
# 			if std_att_inst.has_attended:
# 				pass # should not be passed????
# 			else:
# 				# perform face recognization
# 				try:
# 					# sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
# 					stu_faces = StudentFace.objects.filter(student_id=studnt[0])


# 					# t0 = time.time()
# 					# print("before faces detection of student")

# 					dataset2 = [
# 						'/home/vijay/faces/1.png',
# 						'/home/vijay/faces/2.png',
# 						'/home/vijay/faces/3.png',
# 						'/home/vijay/faces/4.png',
# 						'/home/vijay/faces/5.png',
# 						'/home/vijay/faces/6.png'
# 					]
# 	#-----------------------Itertion from dataset------------------------------------------------#
# 					# queryset = Student.objects.filter(classroom_id=classroomId).order_by('-student_id')
# 					# std_id[index][0]
# 					for s_index in range(30):
# 						path = os.BASE_DIR + join("/base/dataset")
# 						image_url = path+"User"+str(std_id[index][0])+str(s_index)
# 						# filename =  image_url
# 						detected =  face_attend(std_att_inst,instituteId,staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D)


# 	#-----------------------Itertion for student image------------------------------------------#
# 					# print("face len :" + str(stu_faces.__len__()))
# 					for s_index in range(stu_faces.__len__()) :
# 						# reassign
# 						sf = stu_faces[s_index]
# 						image_url = sf.student_face_url
# 						# image_url = dataset2[s_index]
# 						filename = sf.student_face_url.split("?")[0].split("/")[-1]
# 						detected = face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D)
# 						# detected = face_attend2(std_att_inst,instituteId,staffId,subjectId,classroomId,
# 						# 			image_url,filename,class_img_path,
# 						# 			fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D)
# 						if detected:
# 							break
# 						# else:
# 						# 	break

# 					# print(time.time() - t0)
# 					# print("after faces detection of student")

# 				except :
# 					std_att_inst.has_attended_final = 0
# 					std_att_inst.save()
# 					continue

# 		except StudentAttendance.DoesNotExist:
# 			# print('Exception in getting record %s', e)
# 			std_att_inst = StudentAttendance.objects.create(student_id=std_id[index][0],staff_id=staffId,subject_id=subjectId,classroom_id=classroomId,created_date=today,attendance_date=today,has_attended=0,has_attended_final=0,on_leave=0,timestamp=datetime.datetime.now())
# 			# sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]
# 			# image_url = sf.student_face_url
# 			# filename = sf.student_face_url.split("?")[0].split("/")[-1]
# 			# face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename)
# 			try:
# 				sf = StudentFace.objects.filter(student_id=std_id[index][0]).reverse()[0]

# 				stu_faces = StudentFace.objects.filter(student_id=studnt[0])

# 				for s_index in range(stu_faces.__len__()) :
# 					# reassign
# 					sf = stu_faces[s_index]
# 					image_url = sf.student_face_url
# 					filename = sf.student_face_url.split("?")[0].split("/")[-1]
# 					face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D)
# 					continue
# 			except :
# 				std_att_inst.has_attended_final = 0
# 				std_att_inst.save()
# 				continue
			
# 	# save and generate notification to staff
	
# 	try:
# 		notif_url = "teacher-classroom-attendance?id=" + classroomId +"&subid=" + subjectId
# 		print("notification url is " + notif_url)
# 		notif= Notification.objects.create(from_id=0,login_type_id = 102 ,to_id = staffId ,notif_title = "Attendance Complete" , notif_message ="Attendance for your class is completed. Please check.", notif_for_url = notif_url, notif_for = "attendance",is_read = 0,is_notified = 0,created_date = today,timestamp = datetime.datetime.now())
# 	except Exception as e:
# 		print("notification cannot be saved " + str(e))

# 	return True

# #------------------------------------------------------------------------------------------------------------------------------#
# #------------------------------------------Online Student Detection------------------------------------------------------------#
# #------------------------------------------------------------------------------------------------------------------------------#
# @shared_task

# def online_std_att(studentId,instituteId, staffId,subjectId,classroomId,student_url):
# 	fd_method   = 0               # face detection method (0:HOG, 1: CNN)
# 	fr_method   = 2               # face recognition method (0: Dlib, 1: Dlib+, 2: FaceNet)
# 	sc_method   = 0               # 0 cosine similarity, 1 euclidean distance
# 	uninorm     = 1               # 1 means descriptor has norm = 1
# 	theta       = 0.6             # threshold for cosine similarity
# 	print_scr   = 1               # print scores
# 	show_img    = 0               # show images
	
# 	print("[facer] : ----------------------------------------------------------------") 
# 	# print_definitions(fd_method,fr_method,sc_method,uninorm,theta)
# 	print("[facer] : loading face recognition model...") 
	
# 	fr_model = load_fr_model(fr_method) 

# 	today = date.today()  
# 	# try to give path on only one place i.e; globally 
# 	tmp_path = "/tmp/"
# 	# rdb.set_trace()
# 	r = requests.get(student_url, stream = True)
# 	if r.status_code == 200:
# 		# Set decode_content value to True, otherwise the downloaded image file's size will be zero.
# 		r.raw.decode_content = True
# 		# Open a local file with wb ( write binary ) permission.
# 	#	tmp_path = "/home//vijay/prog/python/kgtopg-platform/temp/"
# 		filename = student_url.split("?")[0].split("/")[-1]
# 		with open(tmp_path+filename,'wb') as f:
# 			shutil.copyfileobj(r.raw, f)
# 		print('Image sucessfully Downloaded: ',filename)
# 	else:
# 		print("not done")	

# 	img_session = tmp_path + filename

# 	print("reading session image")
	
# 	S = imreadx(img_session,show_img)
	
# 	print("[facer] : detecting faces in session image " + img_session + "...")
	
# 	faces  = face_detection(S,fd_method)
	
# 	print("[facer] : " + str(len(faces)) + " face(s) found in session image " + img_session)

# 	# computation of descriptors in detected faces of session image
# 	print("[facer] : finding "  + " in session image ...")
	
# 	D = session_descriptors(S,faces,fr_method,fr_model,uninorm)

# 	print('student snapshot file now available..');

# 	class_img_path=""
# 	try:
# 		# attendance query		
# 		std_att_inst = StudentAttendance.objects.get(student_id=studentId,staff_id=staffId,
# 				subject_id=subjectId,classroom_id=classroomId,created_date=today)
		
# 		if std_att_inst.has_attended == 0:
# 			try:
# 				stu_faces = StudentFace.objects.filter(student_id=studentId)
# 				for s_index in range(stu_faces.__len__()) :
# 					# reassign
# 					sf = stu_faces[s_index]
# 					image_url = sf.student_face_url
# 					filename = sf.student_face_url.split("?")[0].split("/")[-1]
# 					detected = face_attend(std_att_inst,instituteId, staffId,subjectId,classroomId,image_url,filename,class_img_path,fd_method,fr_model,fr_method,sc_method,uninorm,theta,print_scr,show_img,D)
# 					if detected:
# 						break
# 			except Exception as e:
# 				print("Student faces objects %s",e)
# 		else:
# 			pass
# 	except Exception as e:
# 		print('Exception in getting attendance record %s', e)
		
# 	try:
# 		notif_url = "teacher-classroom-attendance?id=" + classroomId +"&subid=" + subjectId
# 		print("notification url is " + notif_url)
# 		notif= Notification.objects.create(from_id=0,login_type_id = 103 ,to_id = staffId ,notif_title = "Attendance Complete" , notif_message ="Attendance for your class is completed. Please check.", notif_for_url = notif_url, notif_for = "attendance",is_read = 0,is_notified = 0,created_date = today,timestamp = datetime.datetime.now())
# 	except Exception as e:
# 		print("notification cannot be saved " + str(e))

# 	return True